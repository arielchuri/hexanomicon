# Files:

- Hexanomicon 01 letter and A4
- Hexanomicon formatted for ring binders letter and A4
- Hexanomicon cover spread for printing on colored paper letter and A4
- player map, judge map, world map, month calendar, year calendar, and 2 map sticker sheets in letter and A4
- luck token sheets in letter and A4

# hexanomicon
Welcome to the Hexanomicon git repository.   

- The file [copy/01mainCopy.md](copy/01mainCopy.md) contains the copy. This is the main working file. Please look here for content.  
- [contents.md](contents.md) contains a list of Issue One's contents and the status of each item.   
- *forGloryZine.sla* is the layout file. I am using the program [Scribus](https://www.scribus.net/) to create it.   

## Software
I am trying to use only free and opensource software applications which I run in the KDE Neon version of GNU/Linux.   
The copy was typed with Markdown formatting and then exported to HTML. The HTML was styled with CSS for print output. The HTML was exported to PDF using the Prince command line tool.   
The artwork is sketched in blue pencil and inked with markers. I take a picture with my phone and then message myself using [Matrix/Riot](https://matrix.org/blog/home/).   
[Inkscape](https://inkscape.org/) is used to trace the drawing and create a vector file out of it. The cover image was created using traced art, clip art splatter effects, and shapes created in Inkscape and given a hand drawn look with filters.   
[Krita](https://krita.org/en/) is used for any photo/bitmap needs.

## Typography
The cover uses [Blaktur](https://houseind.com/hi/blaktur) from House Industries which I bought for my [BLACKLIGHT SKULL](https://www.instagram.com/p/BoRyC7BBGaR/) project.   
As well as [Varna](https://www.behance.net/gallery/40488203/Varna) by Eimantas Paškonis which is also used for some of the body copy inside.    
[Gothik Steel](https://www.dafont.com/gothik-steel.font) is used for headlines
International Business Machine's corporate typeface [Plex](https://www.ibm.com/plex/) is available for free.

Thank you. If you have any comments or suggestions please [email me](mailto:forglory@sparklelabs.com?subject=git).  
\_ariel   

![Screenshot of my desktop](meta/screenshotScreen.jpeg)   

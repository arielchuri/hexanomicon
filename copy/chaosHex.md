### Chaos
Chaotic terrain can have a variety of effects. These effects may be ameliorated or enhanced by many factors. The characters may carry extra water or have no water at all. Some of the party may have outdoor or astrological skills or even, a compass. You can add modifiers to the roll or move up or down the dice chain. If the party rolls poorly, they may be able to use magic or ingenuity to limit or negate some effects. Modify these rolls with the party's luck modifier calculated above.  

#### Determine the chaos effect
**Table 2-1: Chaotic Hex Effect**   

|    d20 | result                            |
|--------|-----------------------------------|
| Fumble | reroll d8+1 and add beasts        |
|    2-3 | Sickness                          |
|    4-5 | Thirst or Exposure                |
|    6-7 | Lost                              |
|   8-10 | Storm                             |
|  11-19 | Beasts                            |
|   Crit | Helpful NPC or traveling merchant |

**Table 2-6: Sickness**   
*You feel cold but begin to sweat. Your stomach gurgles. Did you eat something bad?*

|    d20 | Result                                                                                                                             |
|--------|------------------------------------------------------------------------------------------------------------------------------------|
| Fumble | Pestilence - 1 week and Luck check; Pass: 1d3hp and 1d5 Stamina (cannot go below 1 of either), Fail: Reduced to 1hp and 4 Stamina. |
|    1-5 | Plague - 3 day and DC 15 Fort save or 1d3hp and 1d5 Stamina (cannot go below 1 of either).                                         |
|   6-10 | Sickness - 2 days and As above but DC 10                                                                                           |
|  11-15 | Fevers - 1 day and As above but DC 5                                                                                               |
|    16+ | The Runs - 1 day                                                                                                                   |
|   Crit | Develop an immunity to this area's disease.                                                                                        |

**Table 2-2: Thirst**   

*First begins the terrible thirst then the aching head. The sun beats down as you squeeze out the last drops of water. You tongue swells and your mind becomes haunted.*

| d20+luck | Result                                                                                 |
|----------|----------------------------------------------------------------------------------------|
|   Fumble | As above and unluckiest succumbs. Permanently lose 1 Stamina and bedridden for 1 week. |
|       -5 | 1d4hp+1, 2 Stamina                                                                     |
|     6-10 | 1d4hp, 1 Stamina                                                                       |
|    11-15 | 1d3hp                                                                                  |
|      16+ | Desperate, you find a watering hole                                                    |
|     Crit | Healing oasis - Food, water, and all healed                                            |

**Table 2-4: Exposure**   
*You are shivering. Your speech slurs. Clumsy, drowsy you must find shelter.**

|    d20 | Result                                                                                    |
|--------|-------------------------------------------------------------------------------------------|
| Fumble | 3 Stamina and 3 days + 1d4 Damage, unluckiest gets frostbite, permanently lose 1 Agility. |
|    1-5 | 3 Stamina & 3  days                                                                       |
|   6-10 | 2 Stamina & 2  days                                                                       |
|  11-15 | 1 Stamina & 1  day                                                                        |
|    16+ | Find shelter easily                                                                       |
|   Crit | Survivalist +2XP, Each player acquires warm furs (+3 on this table if they keep and wear) |

**Table 2-3: Lost**   
*The area begins to look familiar. You come upon a campsite and realize it was your own.*

|    d20 | Result                                                                 |
|  ----- | ------------------------------------------------------------           |
| Fumble | Lose 1 week, roll on exposure or thirst, encounter beasts.             |
|    1-5 | Lose 3 days suffer 1d3hp exposure and luck check or encounter a beast. |
|   6-10 | Lose 2 days and luck check or encounter a beast.                       |
|  11-15 | Lose 1 day.                                                            |
|    16+ | Find your way again and lose no time.                                  |
|   Crit | Catch site of and move towards your goal. Gain 1 day.                  |

**Table 2-5: Storm**   
*The sky grows black and the wind wails.*

|    d20 | Result                                                                     |
|--------|----------------------------------------------------------------------------|
| Fumble | Maelstrom - Lose 4 days, 1d5hp                                             |
|    1-5 | Hurricane - Lose 2 day, Each PC makes a Luck check or 1d5hp and 1 Stamina. |
|   6-10 | Torrent - Lose 1 day, Luck check or 1d3hp.                                 |
|  11-15 | Storm - Lose 1 day                                                         |
|    16+ | You are very wet                                                           |
|   Crit | Searching for shelter, you find food or helpful NPCs.                      |

**Table 2-7: Doom Hex effects**   

The doom hex represents a particularly dire locale. These areas usually are a lone hex but on occasion may surround an important location. Such a hex could have a particularly vile beast or a dangerous combination of flora and fauna. They could also be combined with the effects below. 

1. Lost time
   The days begin to blur. Lose: 1. d3 years, 2-4. d12 months, 5-8. d4 weeks, 9-10. d7 days   
2. Never ending
   The goal moves. Leaving the hex reenters the hex from the opposite edge. Redoing in hex effects again and again. Reverse course to move forward.   
3. Heavenly
   It feels too good. The hex gives joy and energy. Delicious fruits and warm springs abound you feel you feel stronger. The players gain one random point of attribute for each day they spend here. They lose all upon leaving and gain mutations.   
   
   5 points gained (and lost) bestows a minor mutation, 10 points gives a major mutation, and 15 points gives greater mutation.   

4. Cursed
   You despoil hallowed ground by being here. Curse DCC RPG Core. Maybe removed or avoided by a variety of means. Possibly returning an object to the area or killing something.  
5. Wasting
   Some force drains you. Drains one Strength, Stamina, or Intelligence per day.   

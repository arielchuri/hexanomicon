## Beastomatic
The beasts created here should be applied as the judge sees fit.
On entering a hex for the first time they may only be tested by the beasts. If the players choose to return, the beasts may decide to take action.

Roll one or two items from the following two lists to create creatures like; **Burrowing Slime Women** or **Spitting Rat Monkeys.**
There are alternate names as well.
Some items could provide only for flavor while others may be used, but not be part of the creatures proper name.
Additional names can be found in DCC RPG Appendix S.

The tables or set to have the powers from the first table applied to the base creatures on the second table.
*Acid Dogs* are easier to stat than *Pig Wolves.*
You may interpret the names so that *Magic*, *King*, *Zombie* and *Flower* could become *Lich Orchid*.
*Flying* or *Bird* and *Un-dead* could be *Flying* *Skulls*.   

**Table 4-1: Beast Power**
Name, Special, alternate names. 

1. **Acid** Damaging this creature sprays 1d4 damage for 1d3 rounds unless washed off, plate armor may protect against this damage while non-metal armors may be damaged; +3 Fort.    
   *Caustic, Burning*.   

2. **Ambush** Surprise round, +3 AC, +5 stealth/hide.   
   *Shock, Sneak, Camouflage, Chameleon*  

3. **Beast** Muscular and hairy or deformed, such as faces on torso and no head, +1 HD, +1 DMG, +1 Init, +2 Fort, +1 Ref, -2 Will.    
   *Monster, Monstrous, Fiend, Ferrel, Wild, Mishapen, Demented*     

4. **Blinding** Spit or glowing eyes attack can cause temporary blindness, +2 ranged 30', 1d3 rounds of blindness, extra 1d16  for blinding attack, blinding attack ignores armor.   
   *Shining, Blazing, Spit*   

5. **Blood** If an attack causes damages all beasts target wounded character with +2 and new beasts may appear.   

6. **Brain** Intelligent, possibly psionic. +3 Init, Psionic attack DC 15 Will save or 1d4  damage and lose a turn, +5 Will. 

7. **Burrowing** Can come up anywhere with surprise, +2 Init, may come up in waves.
  If paired with other words like *Brain Burrowing,* Use something like grapple on hit and -1 Intelligence damage.   
8. **Claw** Giant claws. +3 DMG or +d16 Act claw attack, grapple on hit   
37. **Corrupting** Successful attack causes a mutation in 2d4+Stamina days unless magically healed. Based on damage; 1-12: minor corruption, 12-20: major corrption, 21+: greater corruption (or use your own mutation tables).   
   *Radioactive, Mutator, Mutated*   
9. **Crusher** Massive mandibles. Grapple causes 1d6 DMG next turn.     
   *Grapple, Smasher, Hammer**      
10. **Jumping** Jump 40', +1 Init, +4 Ref. Can leap over opponents.   
    *Leaping, *Hopping*   
11. **Death** Only one assailant but death attack causes 2d10 Stamina drain at 2 Stanima/round. Creature fades away after a succesful attack.   
    *Dark*  
12. **Demon** Red with full black eyes. Attack causes Fort save (DC based on attack roll) or paralysis (-d3 , crawl 5', -5 AC, Halflings are immune), +2 Init, +2 Fort, +1 Ref, +3 Will.      
   *Imp, Demonic, Unholy, Cursed, Debased*,      
13. **Doom** Black spikes or flails or blade armor, +2 Damage, Deed die +d1/HD so a 1HD creature has d3 added to action and damage.   
   *Black, Spike, Flail, Impalor*   
14. **Dream** DC 8 Will save or lose a round, every round. +8 DC if you missed your save last round. Nod out. Elves are immune.      
    *Euphoria, Soma, Ambrosia, Poppy, Peyote*     
15. **Electro** Attacks cause additional 1d4 electrical damage to target and all within 5' of anyone who receives electrical damage or all in water. Any melee attack with metal that hits must DC 10 Fort save to hold on to weapon.    
   *Shock, Shocking, Electric, Zap*     
17. **Evil** Fights to death, +2 Init, +1d16 , +3 Ref.    
    *Vicious, Berserker*      
24. **Extra Bits**  
    
    |  d8 | Extra bit       | Effect                                                                                                                                      -                                    |
    |-----|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | 1-2 | Roll twice more | These can stack so that you can get all results if you keep hitting 1 or 2                                                                                                       |
    |   3 | Head            | +1d20 bite if it bites, Init +2, +3 Will, -50% chance to be surprised, any power that requires a Will save or psychic/mental effect is increased +d1 or the DC is increased by 5 |
    |   4 | Arms            | +1d20                                                                                                                                                                         |
    |   5 | Legs            | +20' move                                                                                                                                                                        |
    |   6 | Tail            | +1d20 tail attack (1d5 damage)                                                                                                                                                   |
    |   7 | Quills/spikes   | +d1 damage to attack and/or grapple                                                                                                                                    |
    |   8 | Scales/shell    | +4 AC                                                                                                                                                                            |
    
18. **Fade** Fade to invisible; Melee +2, DC 15 INT check to see it or attack at -d2.   
   You see only a disturbance in the light and footprints created.    
   *Invisible, Inviso, Ghostly*      
19. **Fire** Napalm spray. +3 ranged 20’, catch fire. 1d4 each round until you put it out.  
   *Burning, Napalm, Fire Breathing, Fiery*   
20. **Fear** Morale check for hirelings and 0-levels. DC 20 Will Save or  and spell check -d1.   
27. **Flying** Fly 50'. May have wings, +2 AC and fewer HD or hp.   
21. **Fog** A shroud of fog precedes and surrounds them but does not affect them. Can only be melee attacked and at -1d.   
    *Mist*   
22. **Freeze** Breath weapon, DC 15 Ref save or lose 10’ speed, -2 to attack, 1d4 cold damage. Modifiers last 3 rounds and stack on succesive hits.   
23. **Gas** Poison cloud. May require recharge. 30’ area, centered 15' from attacker and lasts 3 rounds, 1. Poison 1d4/turn 2. Sleep (DC 10 WILL save each round of exposure) 3. Dizzy -d1 actions   
25. **Giant** +50% HD, +3 AC, +d2 DMG, Crit on 19-20   
    *Gargantuan, Humongous, Gigantic, Huge, Towering*
26. **Glam** Illusory appearance. Roll on NPC table or make them beautiful or cute.
28. **Glow** Enveloped in green fire. Infravision is blinded in darkness, -1 Stamina per round to all within melee range. Losing 3 points causes vomiting. Reduced to 3 Stamina causes mutation.   
   *Radioactive, Corrupting*   
30. **Gore** Additional  1d20, Piercing tentacles, 1d4 DAM + grapple & 1d6 DAM/turn until DC 15 STR or tentacles are cut.   
   *Tentacle*
16. **Healer** -1 AC, +1 HD, +3 Fort.   
    This applies to only one of a group. *Laser Slugs* would have one *Healer Slug* with them.   
    
    1. Auto area: 10' radius, 2hp/round.   
    2. Area: 5' radius, 4hp/round as an action.   
    3. Targeted: heal 1 creature for 2 HD as an action.   
    
    *Cleric, Voodoo, Witch Doctor, Shaman*      
31. **Horror** +2 AC, +3 Init, Surprise and disappear in darkness (DC 20 to see or DC 15 with infravision). Humans must DC 15 Will save each round until success or stupor.    
32. **King** +2 Init, +1 AC, +1 HD, MV +20. Only one but has minions which receive +d1 , +5 Will save, pass morale checks while present and fail morale when not. Minions can be normal versions of the same creature or other creatures rolled only on table 4-1.    
    *Chief, Chieftan Boss, Leader*   
33. **Lascivious:** You feel something stir inside you.   
   DC 15 Will save or attempt to give it a hug and must pass DC 10 Will for every attempted attack. Elves are immune.    
   *Lovely, Lonely*   
34. **Laser** Burning rays. 60' ranged (1d4+1).   
   *Ray, Beam*   
35. **Mad** -1 AC, -1 Init, If a melee attack hits it attacks another target in melee range. Continues until miss.   
    *Insane, Berserker*
49. **Magic** These creatures may have innate magical abilities or they have abilities similar to magical abilities.    
Spells other than *anti-magic* have a 50% chance of success and are lost on failure.   

| d10 | Spell                  | Spell Check                                           |
|-----|------------------------|-------------------------------------------------------|
|   1 | Roll twice more        |                                                       |
|   2 | Chill touch pg. 133    | SC 17                                                 |
|   3 | Chocking cloud pg. 134 | SC 13                                                 |
|   4 | Color spray pg. 135    | SC 13                                                 |
|   5 | Flaming hands pg. 142  | SC 17                                                 |
|   6 | Magic missile pg. 144  | SC 13                                                 |
|   7 | Magic shield pg. 146   | SC 13                                                 |
|   8 | Sleep pg. 156          | SC 13                                                 |
|   9 | Phantasm pg. 187       | Illusion of larger creature or NPC                    |
|  10 | Anti-magic             | Blocks all magic and negates bonus from magical items |

*Wizard, Sorcerer, Arcane* Additional names per spell's name or manifestation.   

29. **Magic Drain** Magical weapons that hit this creature heal instead of damage and lose their magic until the next day. Many spells have no effect and spells that cause damage heal. May even add HD and increase in size.   
36. **Mind** What are these whispers in my head?   
    DC 15 Will save or attack a friend at -2 melee until passing Will save.   
    *Mind Control, Psychic, Psionic*
38. **Pain** Attack causes -1d  and -5 Reflex until any healing.   
39. **Phase** You see them and they fade away only to reappear closer. 
   Phases in and out of reality. +4 AC, Melee attacks DC 15 FORT save or stun for 1 round and ignore armor/shield except magical. +6 Reflex
   *Spectral, Warp*   
40. **Plague** Bleeding sores and foaming mouth, weeping fleas engulf you.   
   Everyone in melee range must make a DC 15 FORT or become ill.   
   d3: 1. Dwarves, 2. Elves, 3. Halflings, are immune.   
   No immediate effect but lose 1 Stamina/day for 1d20 days (judge may roll in secret). -1d /day after 3 days. Bedridden and require care below 6 Stamina (-3 Stamina/day without care). Death at 0 Stamina. Anyone caring for you must pass DC 10 Fort save or become ill as well.   
   These crazed creatures always pass morale checks.   
   *Infectious, Sickening, Pestilent, Calamity, Rabid*   
41. **Poison**   
    Method (if needed) d3: 1. claw or bite or weapon, melee; 2. spray or gas, 10'x10' centered 5' away, Ref save; 3. spit, ranged 30'    
    Resistance d7: 1. None 2. Dwarves 3. Elves 4. Halflings 5. Human females 6. Thieves    

| d8 | Fort  | Pass             | Fail                                                                                   | Alternate names      |
|----|-------|------------------|----------------------------------------------------------------------------------------|----------------------|
|  1 | DC 10 | 1 Stamina        | 1d6 Stamina and 1d3hp                                                                  | Draining, Wasting    |
|  2 | DC 10 | 1 Agility        | 1d6 Agility                                                                            | Tremor               |
|  3 | DC 10 | 1 Intelligence   | 1d8 Intelligence                                                                       | Stupefying, Stunning |
|  4 | DC 10 | -1 Strength      | -2d3 Strength                                                                          | Weakness             |
|  5 | DC 5  | Blind 1d3 rounds | Blind (permanent)                                                                      | Blinding             |
|  6 | DC 15 | No effect        | Paralyzed 1d3 rounds                                                                   | Paralyzing           |
|  7 | DC 12 | 1d4 damage       | 1d3 damage/round for 1d6 rounds                                                        | Wasting              |
|  8 | DC 6  | No effect        | 2d8 damage (if not reduced to less than 0hp, permanent +1 stamina after fully healing) |                      |

   *Black, Widow, Death, Viper* for more names see individual poison type. 

42. **Puss** Skin taught over bloated body.   
   Explodes when killed causing 1d4 in a 5’ radius. Can cause a chain reaction.   
   *Exploding, Bursting, Burst*   
43. **Ram** Charge attack: +2 to hit and damage, AC -2 until next round, must move at least 5', targets who take more than 3 hp damage must DC 10 Ref save or prone.   
   *Rush, Push, Slam, Bash, Charging*  
44. **Rune** Glowing runes. Regenerates 2 hp every round, as an action it can regenerate 1 HD.   
45. **Sleep** Song or smell causes sleep (DC10 WILL). Elves are immune.   
46. **Slime** Touch partially envelopes, DC 15 Reflex save to avoid, 1d3 dissolve damage/turn, damages all organic matter, completely enveloped on following turn depending on size of creature, blunt weapons stick, enveloped PCs take half damage from attacks on slime creature, -3 Reflex.   
   *Goop, Glue, Gelatinous, Gel, Blob, Jelly*  
47. **Slow** Attack or area effect causes -10’ Speed, -3 Init, -4 Reflex for 1d10 turns   
48. **Snipe** Shoots darts or quills from up to 90'. May have limited ammo or require recharge.   
   *Sniper, Missile*
50. **Speed** +20’ move, +3 Init, +4 Reflex  
   *Hyper*
51. **Spider** +20’ move climb, 30' jump, +3 Init, may shoot sticky web (see below).   
52. **Spitting** 30’, DC 15 Ref save, 1. Poison (see above) 2. Acid (see above) 3. sticky (-d1 ) 4. Attractant (summon more) 5. Stink (lose a round and stink for d16 days) 6. Drug   
53. **Spore** releases cloud of spores from holes in body. -1 on all rolls from choking or obscured by cloud. Humans are allergic, DC 15 Fort save or lose a turn. 
54. **Swarm** Smaller or normal size version as a single swarm. Multiply quantity by hp to get total hp. All in 40' wide circle are attacked each round for half damage. DC 12 Strength check or non-flying creatures drag you down for -2 AC and -d1 . DC 12 Will save against flying creatures or become disoriented for -2d  next turn (halflings are immune). Attacks that are not area effect do half damage.    
55. **Vampire** +3 Init, +3 Bite (1d4) auto grapples (1d3hp/round)   
   *Blood Sucking, Blood Sucker, Leech*
56. **Vile** DC 15 Fort save all within melee range or wretch 1 turn. Roll each turn until you make the save (Dwarves are immune).    
    *Disgusting, Gross*
57. **Wasting** Exudes black aura. You feel weaker. -1d to all actions and -1 Stamina/round for each round in melee range. Does not stack.    
    *Draining, Doom*  
58. **Web** Shoots a sticky web at a 10' square, DC 15 Reflex save to avoid. Move reduced 15', -1d to attack, +1d to be attacked. DC 15 Strength to break free.   
59. **Wraith** Attracted to lucky PCs. Attack does luck damage. Unconscious at 0 Luck. Any one left unconscious with the Wraiths becomes a *Luck Zombie*.   
60. **Un-dead** Appearance and traits *DCC RPG core rulebook* Page 381. + Attack, +1 HD, -2 AC, -3 Reflex. Does not eat, drink, breath, or sleep. immune to *sleep*, *charm*, *hold*, mental effects, and cold damage. May have other effects for *Skeleton*, *Mummy*, or *Ghoul*.    
   *Zombie, Skeleton, Grave, Soulless, Undying, Eternal, Ghoul, Mummy, Ghoulish*
   

**Table 4-1 Beast Type**

1. **Ant** (1d8+10): Init +0; Atk bite +2 melee (1d3 plus latch); AC 14; HD 1d4; MV 20’ or climb 40’ (difficult terrain that may slow players allows ants to use their climb speed); Act 1d20; SP Latch; SV Fort +5, Ref +1, *	Will -3; AL L. Successful attack latches on. -AC and -d1  per ant until dc10 *	STR to shake one off or dc 20 to shake all off.    
2. **Ape** (3d3): Init +1; Atk bite +2 melee (1d4+1) or slam +3 melee (1d6); AC 10; HD 1d8+2; MV 20' or climb 30'; Act 1d20; SP +5 to hide checks in jungle terrain; SV Fort +4, Ref +2; AL N.   

3. **Bats, Giant** (3d3): Init +4; Atk bite +3 melee (1d5 + disease); AC 13; HD 1d6+1; MV fly 40’; Act 1d20; disease, no natural healing for 1d14 days; SV Fort +3, Ref +10, Will -2; AL L.        

4. **Bear** (1): Init +2; Atk bite +2 melee (1d6 plus grapple) and claw +4 melee (1d4+1)+ shove; AC 13; HD 3d8; MV 40'; Act 1d24 (crit on 23-24), 1d16; SP maul, shove; SV Fort +1, Reflex +1, Will -2; AL N.
   Maul for additional an 1d8 damage if bite victim remains grappled next turn, claw auto hits and crits on 16. Shove: If not grappled and claw causes 4 or more damage, victim is prone.    

   *Monster, Beast, Giant*     

5. **Beetles** (2d5+5): Init -2; Atk mandibles (1d3+1); AC 15; HD 1d6; MV 20' or climb 20' or Fly 30'; Act 1d20; SV Fort +1, Ref +0, Will -3; AL N.   
6. **Birds, Large** (3d3+3): Init +3; Atk claw +6 melee (1d4); AC 16; HD 1d5; MV fly 40’; Act 1d20; SV Fort +0, Ref +6, Will -2; AL N.   
   Birds appear in small numbers and slowly grow as more join, only attacking in sufficient numbers.   
   *Flock, Murder, Host, Crows*   
7. **Cats** (2d3): Init +1; Atk claw +2 melee (1d3) or bite +3 melee (1d5); AC 12; HD 1d8; MV 40' or climb 20'; Act 1d20; SP *pounce*; SV Fort +2, Ref +3; AL N; Crit M/d8   
   If the mountain-lion cougar makes the first attack of combat, it will pounce; otherwise it attacks normally. Thereafter, it will alternate attacks between claw and bite, pouncing when possible.    

   *Pounce* The mountain-lion cougar can pounce to gain an extra d20 attack die and attack that round with both a claw and bite. The mountain-lion cougar can only pounce if it surprises its victims, attacks first due to initiative, or has taken no damage since its previous attack. [Moutain Lion Cougar](https://diyanddragons.blogspot.com/2018/12/gfa18-mountain-lion-cougar.html)   

   *Pride, Tigers, Lions, Panthers, Cougars, Jaguars, Kittens*   

8. **Crabs** (2d3+2): Init +1, Atk 2x +3 Claw (1d4+1); AC 15 (7 on underside); HD 1d8; MV 30'; Act 2d20; SV Fort +4, Ref -1, Will -2.   

11. **Centipedes, Large** (4d3): Init +1; Atk bite +6 melee (1d6); AC 14; HD 1d4; MV 40' Climb 40'; Act 1d20; SV Fort -1, Ref +2, Will -1; AL C.    

12. **Dogs** (1d6+6): Init +2; Atk bite +4 melee (1d3+1); AC 11; HD 1d5; MV 35'; Act 1d20; SV Fort +2, Ref +3; AL N.

    *Hounds, Puppies, Mongrels, Mutts*

13. **Elves** (2d4+1): Init +2; Atk weapon +1 melee or +2 ranged; AC 11; HD 1d8; MV 30’; Act 1d20; SP hide; SV Fort -2, Ref +1, Will +4; AL C.       

    Twisted versions of Elves. Immune to magical *charm,* *sleep,* ect. Can see through most glamours and illusions. As an action they can hide in their home landscape, DC 20 Int check to spot one. May have crude weapons and crude armor.  

    *Faeries, Sprites, Spirit Folk*      

14. **Flowers, Giant** (1d3+3): Init -1; Atk leaf slash +3 melee 10' reach (1d4+1); AC 9; HD 1d30; MV 0'; Act 2d24; SP scent attractant, flora; SV Fort +0 but immune to many effects, Ref -5, Will +10; AL C.   
    
    Giant flowers seem harmless enough until you find yourself in the middle of them.
    They open their scent sacks beckoning unsuspecting creatures for a taste.
    Those who succumb walk slowly towards the flower (half speed), pushing away in who would stop them.
    They dunk their head into the flower and hang their limply until dead. 
    
    DC 15 Will save, victims may get another save if someone tries to help them or there was a distraction as they walk towards the flower. 
    
    **Flowers, Normal** 
    More an environmental effect than a beast. 
    You feel sleepy crossing a 200' field of flowers. 
    100' feet in requires a DC 7 Will save every 10' or lose 1d3 Int.
    The DC increases by 2 at each failure.
    If Int falls below 3, fall asleep.
    Either be rescued or wake up 12 hours later losing 1 Int permanently.
    Elves may be immune.
    
    Flora are mindless and enjoy similar powers to undead but are susceptible to critical hits. Flora takes double damage from fire and half damage from peircing or bludgeoning attacks.
    
    *Poppies, Orchids, Roses, Lilies, Lotus, Geraniums*   

10. **Ghost**: Use stat block or powers or both. Init +2; Atk special (see below); AC 10; HD 2d12; MV fly 40’; Act 1d20; SP un-dead traits, immune to non-magical weapons; SV Fort +2, Ref +4, Will +6; AL C.    

    | d4 | Power              |                                                                |
    |----|--------------------|----------------------------------------------------------------|
    |  1 | Psychic Scream     | DC 15 Will save or 1d4 Damage.                                 |
    |  2 | Telekinetic Attack | Objects from random locations, missile +5 Ranged (1d4 damage). |
    |  3 | Madness            | DC 15 Will save of 1d3 to Intelligence and lose a round.       |
    |  4 | Invisibility       | Turn invisible as an action and reappears to attack.           |

    *Phantasms, Spectres, Spirits, Ghostly*

15. **Gnomes** (3d4): Init +1; Attack 2 daggers (1d4 damage) or fists (1 damage); AC 12; HD 1d4; MV 20'; Act 2d16; SP crit, parry, fairy glow; SV Fort +2, Ref +3, Will; AL C.    
    These creatures often rely on magical powers or trickery and traps rather than physical abilities.
    They usually have more than one of the magical **Beast Powers.** 
    When fighting they crit an any sixteen and fumble only on double 1.
    Gnomes can disengage from melee as an action.
    Anyone touched by a gnome gains *Fairy Glow* and suffers -2 AC for the day or knight.   

    *Kobolds, Leprechauns, Dwarves, Hobbits, Lilliputians, Munchkins, Pixies* 

16. **Goblins** (1d4+4):  Init -1; Atk bite -1 melee (1d3) or as weapon -1 melee; AC 10 + armor; HD 1d6-1; MV 20’; Act 1d20; SP infravision 60’; SV Fort -2, Ref +1, Will -2; AL L. DCC RPG Pg. 417.   

18. **Lizards** (1d3+1): Init +1; Atk bite +1 melee (1d8 + grapple) or tail +3 melee (1d6); AC 16; HD 3d8; MV 20', swim 40', Act 1d24; SP tear; SV Fort +3, Ref +1, Will -4; AL N.    
    Grappled opponents are dragged away next turn and torn apart for 1d10 damage.
    If used as a **Beast Power** rather than **Beast Type** (i.e. *Lizard Men*), +2 Damage and +2 AC and camouflage (DC 15 Int check to see in natural environment).

    *Gila Monsters, Alligators, Crocadiles, Gators, Crocs, Dragons, Worms*   

19. **Men** (4d4): Init -2; Atk club -1 melee (1d4-1) + grapple; AC 10; HD 1d8; MV 30’; Act 1d20; SV Fort -1, Ref -2, Will -1; AL C.    
    May have other simple or stolen weapons and armor.   

    *Thugs, Cannibals*   

20. **Monkeys** (3d3x2): Init +1, Atk claw/bite (1d4-1), missile 30' ranged (1d3); AC 13; HD 1d6; MV 30' Climb 30'; Act 1d20; SV Fort +3, Ref +4, Will -3; AL N.   

    Devious and mean, monkeys leap from trees to attack. Leaping attacks are +d1. Successful attacks grapple. Grappled opponents -1AC/monkey. Each monkey takes a DC 8 Strength action to remove. DC 18 to remove 4. Attached monkeys attack with +d1. Grappled opponents are targeted by other monkeys but not with missile attacks. A monkey has a 1/4 chance to have a rock if it has not melee attacked. All monkeys have an unlimited supply of scat but it does no damage. Fire damage causes a morale check.   

21. **Plants** (1d6+3): Init -1; Atk slash +0 melee (1d5), vine +1 grapple 15' reach (drag in 5'); AC 14; HD 2d8; MV 0'; Act 1d24 slash, 2d16 vine SV Fort +6; Ref -6, Will -3; AL C.     

    This evil flora does not present a threat until the party is in the center of a group. Vines reach out to pull victims in to suffer slash attacks. Vines ignore armor.   

    Flora are mindless and enjoy similar powers to undead but are susceptible to critical hits. Flora takes double damage from fire and half damage from peircing or bludgeoning attacks.

    *Any plant name Cacti, Verbana, Brush, Thistles*   

22. **Pigs** (Special): Init +2; Atk bite +4 melee (1d3); AC 11; HD 1d5; MV 35'; Act 1d20;SP *Charge*; SV Fort +2, Ref +3; AL N.  

    Charge attack: +2 to hit and damage, AC -2 until next round, must move at least 5', targets who take more than 3 hp damage must DC 10 Ref save or prone.   

    Roll d6. Your roll is the amount, the remainder is HD. So a roll of 2 would mean two 4HD pigs. 1HD minimum.    

    When used as a **Beast Power** (*Zombie Pig Men*) rather than a **Beast Type**, use the **Beast Power:** *Beast*.   

    *Boars, Hogs*    

23. **Rats, Ususual Size** (1d6+6):  Init +4; Atk bite +2 melee (1d4+1 plus disease); AC 13; HD 1d6+2; MV 30’ or climb 20’; Act 1d20; SP disease (DC 7 Fort save or additional 1d6 damage); SV Fort +4, Ref +2, Will -1; AL N. (DCC RPG Pg. 484)   

    *Vermin, Rodents, Nutria, Moles, Possum, Trash Pandas*   

24. **Serpents, Constrictor** Init +4; Atk bite +6 melee (1d4 + constrict); AC 14; HD 2d8; MV 30'; Act 1d20; SP constriction 1d5; SV Fort +6, Ref +3, Will +2; AL N.   

    **Serpents, Poisonous** Init +8; Atk bite +4 melee (1d3 + poison); AC 12; HD 1d8; MV 40'; Act 1d20; SP see *Poison* above; SV Fort +1, Ref +6, Will +2; AL N.   

    *Snakes, Cobras, Vipers*

9. **Slugs** (2d3): Init -6; Atk acidic touch +3 melee (1d4); AC 18; HD 2d6; MV 10’; Act 1d20; SV Fort +5, Ref -6, Will -2; AL C.   
   Slow but covered in slime and leaves a trail which may enhance their *Beast Power*.   

    *Nudibranch, Leech, Worm, Amoeba*   

25. **Spiders, large** (2d4): Init +3; Atk bite +2 (1d3 + poison *see above*); AC 14; HD 3d5; MV 40', climb 40', jump 30'; Act 1d20; SV +4, Ref +8, Will -4; AL C.   

17. **Trees** (2d10): Init -3; Atk branch -2 melee (1d4-1); AC 16; HD 3d8+10; MV 0'; Act 4d20; SV Fort +10; Ref -10; Will +10; AL N.

    Trees attack with their branches and, usually one must make a fighting escape as you run through them.

    Flora are mindless and enjoy similar powers to undead but are susceptible to critical hits. Flora takes double damage from fire and half damage from peircing or bludgeoning attacks.

26. **Wolves** (1d6+3): Init +3; Atk bite +2 melee (1d4); AC 12; HD 1d6; MV 40’; Act 1d20; SV Fort +3, Ref +2, Will +1; AL L. (DCC RPG Pg. 431)   

    *Pack, Coyotes, Hyenas, Dingos*   

27. **Women** (3d3): Init +2; See below; AC 12; HD 1d7; MV 30’; Act 1d20; SV Fort +1, Ref +2, Will +2; AL C.      
    Straight out of the male subconscious. Magic may only affect males.   

    - Skin: 1. scales +4 AC, 2. fish scales, 3. Feathers, 4. ink black 4 violet, green, orange, white, normal.
    - Hair: 1. Snakes (grapple = bite +2 melee, 1d3 + poison DC 15 Fort or 1d5 damage), 2. Tentacles (+4 grapple = free bite, attempting to breaking free inflicts 1d3 damage), 3. Long dreds, 5. Blue, 6. Green 7. Bright red, 8. Black   
    - Eyes: Choose color, insect, goat, frog, cat, solid color, not two, and/or glowing   
    - Other features: Horns, antlers, antennae (insect), antennae (alien) 

    | d5 | Weapon                                                                           |
    |----|----------------------------------------------------------------------------------|
    |  1 | Roll twice on Magic table & fangs/claws                                          |
    |  2 | Fangs/claws, bite +3 melee (1d6 damage) and claw +5 melee (1d4 damage);  2d20 |
    |  3 | Roll once on Magic & Dagger +5 melee (1d4)                                       |
    |  4 | Poison dagger +5 melee (DC 15 Fort or sleep)                                     |
    |  5 | Poison dagger (DC 15 Fort save or 1d6 damage)                                    |
    |  6 | Whip +5 melee, 10' reach (1d6 or DC 15 disarm or DC 15 grapple)                  |
    |  7 | Short sword +5 melee (1d6)                                                       |

    *Coven, Hags, Witches*

28. **Worms** (1d3x5): Init -1; touch +3 melee (magical or physiological effect or 1d4 damage); AC 12; HD 2d8; MV 30'; SP morphogenesis; SV Fort -2, Ref +1, Will -1; AL N.    

    Morphogenesis: Any worm cut with a slashing instrument takes the following round to become two worms.    
    
    *Tape, Flat, Planarium*

29. **Mushrooms** (2d4)Init -5; slam +5 melee (1d5 damage); AC 12; HD 4d6; MV 20; SV Fort -2, Reflex -6, Will -4; AL N.    

    These creatures are found in a field of puffballs. Attacking them requires a DC 10 Ref save or set one off.
    
    Effects of eating fresh mushroom: Roll 1d20+15+Luck on *DCC RPG Pg. 223* and then random.

    **Puffballs** Spore explosion DC 15 Reflex or 1d3 damage and another DC 10 Reflex or you set off another one (continues indefinitely on failure).   
    Halfway through a 200' field of these strange orbs one explodes hitting the unluckiest party member.
    Point person must weave there way through the orbs for the next 100'.
    DC 10/round for normal movement, DC 15 for double, and DC 5 for half. 
    All who follow need only a single DC 10 to cross the entire field.
    d10 to determine where any failures occured.
    Last person to leave the field must make a luck check or set off a puffball.

    *Shrooms, Funghi, Fungus, Deathcaps*
    
30. **Vines** Strange vegetation fills this area. Vines attack suddenly but only once. Each character is attacked by 1d8 vines. DC 15 Ref save for each vine. The vines that attack successfully pull taught, opposed Strength with an additional +2 per vine. If a blade is available, a luck check may allow its use. Anyone helping someone caught by vines is attacked by the vines. Torches or any fire source scares vines away.   

**Table 4-5: Weakness or Phobia**

1. Water
2. Fire
3. Cold
4. Wood
5. Elves
6. Halflings
7. Dwarves
8. Women
9. Gold
10. Aggressive behavior
11. Magic power
12. Light
13. Dark
14. Paper
15. Affection
16. These violet glowing crystals
17. Those green mushrooms over there
18. Oil
19. Pony, Horse, Mule
20. Honey
21. Stinky Cheese
22. Fruit
23. Mithril
24. Falcons
25. Hens
26. Night Soil
27. Dolls
28. Dogs
29. Gems
30. Pushcart or other mechanisms

**Table 4-6: Treasure**
Most chaos wildlife does not have treasure but some may have some at their lair and a few may carry some on them. Choose a die on the dice chain to limit the treasure value.

1. Potion (1d16 DCC RPG Pg. 446)
2. Shiny bits, d20: 1. Ring -5 Luck, cannot be removed; 20. Emerald (100 gp).
3. 1d3 Rations
4. 2d4 Rations
5. 1d30 sp
6. 1d10 gp
7. Dagger
8. Holy symbol (20 gp)
9. Useful herbs
10. 1d100 gp
11. The journal of a fallen adventurer
12. Vial of 4 Rubies (50 gp each)
13. Map
14. Strange writing (spell)
15. Scroll (Contents per DCC RPG Pg. 373)
16. Potion (d20+15+Luck mod on DCC RPG pg. 223)

**Table 4-4: Encounter Type**
These beasts will typically have a signature introduction. The judge is invited to create a scene appropriate to the creature. *Gloom Worms* attack at night. Randomly determine who is on watch and go from there. *Glam Elves* appear as cute woodland faeries that eat you. The Fist time you meet a beast should be special. You can also use the table below liven up further encounters. 

Roll d20 or d14+3 (less extreme) or 3d6 (favors center) +/- Luck Modifier.   
You could also d14 for trouble or d14+6 for an easy time or d10+5 for an ok time.   

1. Doom
2. Surrounded
3. Hunted
4. In their territory
5. Surprised
6. A trap
7. Cornered
9. Attacked from behind
10. Difficult terrain
10. Attack
11. Attack
12. Your are tested
14. A beast is surprised
13. You see them first
15. An NPC is under attack (choose from NPC table)
16. A beast is curious
17. Beasts fighting
18. A beast does not see you
19. A beast needs help
20. A beast in the distance

### Beast Levels
These beasts are formulated for lower level characters. The beasts that inhabit a hex do not change as the characters gain experience. These lower level beasts simply avoid the PCs as they pass through. Further hexes may contain higher level beasts. Starting at level 3-4, Add +1 per additional PC level to the AC, to hit bonus, and saving throws. Add +d1 to damage. Add .5 per PC level to HD.   

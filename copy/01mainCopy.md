# The Void
[theVoid.md](theVoid.md)
# 1: Cartomancy
Determine the starting hex. This may be the home village and the surrounding six hexes of fields and hills or forests. You can also fill in hexes for the funnel location, the path there, and an surrounding hexes that may be visible by line of site. Seeing out of swamps or forests is unlikely. Distant mountain hexes may be visible.

## Impassable Terrain
After the players choose a direction of travel, and they are not following a road, they may discover that the path forward is impassable. They remain in the same hex and all of that hex's effects are reapplied. Impassable terrain may be predetermined by the judge in order to make an area or an objective more difficult to reach or it may be determined randomly. Lawful hexes are exceedingly rare and sometimes completely surrounded by impassable terrain.

##### Table 1-1: Impassable Terrain
| Current hex alignment       |          |
|-----------------------------|----------|
| Neutral                     | 1 on d10 |
| Chaotic                     | 1 on d7  |
| Mountanous                  | -d1      |
| Connected border impassable | -d4      |

## Determine the Alignment of the new hex
Base the alingment of the hex either on the requirements of the story or luck. If, in the bard's version of this story, the journey would not warrant even a mention, then it will be neutrally aligned. If they are heading to some goal or retreiving a treasure then it will be chaotic or worse. Any journey of worth will require crossing dangerous ground but you may deem to let the table below decide.

##### Table 1-2: Hex Alignment
Possibly modified by luck or amount of luck burned.

| d20    | Hex Alignment |
|--------|---------------|
| Fumble | Doom          |
| 1-11   | Chaotic       |
| 12-20  | Neutral       |
| Crit   | Lawful        |

## Determine the terrain
The choice may be limited by the die you choose to roll with. Using a d6, d8, or d10 will allow for different possible results.

##### Table 1-3: Topography

| Roll | Landscape                  |
|------|----------------------------|
|    1 | Doom Hex or Plains / hills |
|  2-4 | Plains / hills             |
|  5-6 | Forest / Jungle            |
|    7 | Mountain Range             |
|    8 | Desert / Ice               |
|    9 | Swamp / Bogs               |
|   10 | Lake or Sea                |

Fill in some nearby hexes with the same content or use impassable terrain to completely or partially surround goals or show cliffs and canyons. Large rivers are also drawn between hexes and present some difficulty for the party to cross. Possibly the cost is days as the party builds a raft or looks for some other means to cross. You can fill in more hexes if it will be a long and arduous journey but a hex is not a set distance.   
River names: Course, Estuary, Kill, River, Run, Stream, Tributary, Vein.     
## Determine the effects of the terrain
### Time
Mark off the number of days it takes to traverse the hex on your calendar. Plains and hills 1 day, forest 2 days, swamp and desert 3 days, jungle 5 days, mountains 8 days. Chaos and Doom hexes May take half a day or a weeks to cross. Notes these on your map key.   
# 2: Chaos
[chaosHex.md](chaosHex.md)
# 3. Names
[names.md](names.md)
# 4. Beastomatic
[beastomatic.md](beastomatic.md) 
# 5. Neutral Hex Encounters
[neutralHex.md](neutralHex.md)
# Death by Hex
Our story is not likely to end on a hex. Random events can multiply to create effects that are insurmountable. There are generally two ways to avoid a TPK in such situations. Run or rescue. 
## Run
After disengaging from melee, each PC makes a DC 10 Reflex save to break away from the enemy. Failure means at least one enemy catches up with that PC. They can try again next round at DC 5 and be aided by missile fire or other means. A distraction may also help. If they fight, more of the enemy will catch up. 

Faster enemies will still be on your tail. Some options are:

1. Enter a new hex type. Beasts tend to not venture out of their home environment. PCs must make a DC 5 Reflex just at the hex's edge or trip. 
2. A ravine with a fallen tree across it. DC 5 Reflex to get across the log. Failure means luck check to hang from the log and an enemy may catch up. Failure means into the drink or die. Once you cross over, DC 15 Strength check to dislodge the tree or fight them as they come across. 
3. A cave offers some respite. Fight them at the mouth of the cave or something in the cave scares them away. 
4. Scramble up a cliff face. DC 5 Reflex or fall for (1d3-1)d6 damage and they are upon you. 
5. Jump off a cliff into a river that flows between hexes. DC 10 Reflex save or 1d3 damage. Wash up downriver. 
6. Swim a large river that flows between hexes. Lose anything heavy. 
7. Come upon a settlement **Table 5-2.** DC 5 reflex as the settlement comes into view. 
## Rescue
Some situations will require rescue. It is difficult to outrun a field of *Dream Poppies.* You may be rescued right away or after waking up in the middle of the ritual. Your savior need not be beneficient. The *Evil Flying Monkeys* that save you can take your sleeping bodies straight to the castle of the *Black Witch*. In addition to the options below, a player' patron could intercede on their behalf.   

1. Predator becomes prey. You hear a noise. A look of fear comes over the face of your enemy. A much larger beast comes through and chases your assailants away. You could roll up a Some Lizards or Bears from above.
2. The Servants of, or a powerful NPC/creature saves you.   
    1. A chaotic master drags you out of the frying pan for
         1. An experiment
         2. A wedding (your own)
         3. Trial by combat
         4. Something you know or own
    2. A neutral master comes to your aid 
         1. For a mission
         2. For payment
         3. Just because
    3. A Lawful master rescues you
         1. Because she watches over these lands. 
         2. Happened to be in the area
         3. Knows that your fate is not complete. 
         4. A mission
3. A scouting party of rangers
4. Mounted, armed forces passing through
5. Powerful monks 
6. Bandits who proceed to rob you
7. A band of 0 levels looking for adventure

# Provisions and Supplies
You may wish to keep track of supplies. This can work with an inventory slot system of managing encumbrance. One option is to have a single supply unit for both and food and water. The party is assumed to be replenishing supplies as they travel unless they are in hostile (chaotic) terrain. If in hostile terrain they should cross off one supply unit per day. Anyone out of supplies no longer heals. Additionally, if undergoing thirst, exposure, or sickness each member can cross off one supply unit to add 5 to their thirst, exposure, or sickness roll. Similarly, running out of supplies may add to these rolls.

# Psychogeography
Ley lines, songlines, or dream tracks, these telluric currents cross the earth in patterns without repetition. They spring from and back to the Umbilicus Mundi. A place without location. In regards to applied geomancy, their use is a guarded secret. 

Players would not know that the lines can affect spell checks and not necessarily for the better. Points of intersection are lenses of disturbance. No one can say what the results of practice will be at these locations. The ancient huacas sometimes found there serve to temper or celebrate the chaos.

Points of focus are harder to locate. These may have been marked by subtle geoglyphs that point out the location from afar. Distant from the chaotic influence of telluric cross currents, they can increase a wizard's arcane powers.

The Judge should take care to add the psychogeographical modifiers to spell checks even if the players do not know why.

The psychogeographical map lies under the physical map and can only be discerned through testing (casting spells at particular locations) or through the *Divination* spell.

You may keep track of ley lines on your Judges map. You could draw them in as you please or let it be random. 

**Column** d9: 1 A, 2 B, 3 C, 4 D, 5 E, 6 F, 7 G, 8 H, 9 I   
**Direction** d3: 1 NW, 2 W, 3 SW   
**Occidental crosscurrent** d20-1: 0-19 hexes away   
**Oriental crosscurrent** d20-1: 0-19 hexes away   

If any crossing ley line appears on the map, determine its direction as well and use a d100 for crosscurrents.

## Ley Line effects
Casting a spell on a Ley Line adds 1d3 to the spell check. Ley line intersections and focal points have additional effects. 

### Ley Line Intersection
Intersections have a 50% (+10%/Luck mod) chance of having a positive or negative affect. Roll a d6, add or subtract it from the spell check and look it up on the following table. Add +1 for each additional Ley line at this intersection. 

1. Random spell misfires (lvl 1)
2. 2 random spells misfire (lvl 1 & 2)
3. 3 spells (lvl 1, 2, & 3)
4. 3 spells and phlogiston disturbance
5. 3 spells and phlogiston and Minor Corruption
6. As above but Major corruption
7. As above but Greater corruption 
8. As above and something is released
9. As above and hole is opened

### Ley Line Focal Point
Points of focus occur equidistant from intersections that are at least 4 hexes away. The intersections must be 8 hexes apart to have a focal point. Focal points add 3d5 to the spell check. 

## Divination Spell
Level: 0, Range: 60' or more, Duration: 2 turns, Casting time: 2 actions
### General
A spell to divine the locations of ley lines or other geological features such as an underground water supply.

This is one of the simpler spells and is used by some of the more learned shaman or fortune tellers to find underground wells or mining locations. Wizards may learn this spell at first level as well as Clerics who follow gods of earth, nature, or secrets. When cast by a true magic user the spell’s benefits increase as do any ill effects. 

The cleric or wizard must choose a target such as ley lines, an underground water source, an underground cavity, or a deposit of a particular mineral. The magic user must then concentrate for 2 actions to cast this spell. Additional concentration time adds +1 to the spell check.

A Divining Rod adds +1 to this spell check and Dwarven shaman receive a +1 to this spell check.

If you are judging by the seat of your pants and wish to have a ley line nearby but have not determined the location of any ley lines, roll 1d10 (reroll 10s) to determine the column, d6 for row, and d3 for direction

**Manifestation** See Below

### 0-Level
Typically the caster walks seemingly randomly and, either senses the target below them or feels as if their divining rod suddenly points down. 

**1** The caster falsely senses a target below them at a random location.  
**2-9*** No effect.   
**10** If the caster passes over a target, she feels its presence.   

A 0-level may only cast this spell once per week. Casting rate and spell check may be affected by certain inborn talents, or rituals.

### Magic User
On a result of natural 1, a wizard suffers a 50% chance of major corruption or misfire, rolling on the generic tables as appropriate.

**1-11** Failure   

**12-13** The caster can determine if there is a ley line in the current hex or an adjacent hex though not which adjacent hex. The caster can determine if the targeted geological feature is nearby or its general direction if it is in the same hex.   

**14-17** The caster can determine if their are any ley lines in the current hex or surrounding 6 hexes and their direction.
The caster can determine direction and distance of the targeted feature in this space.

**18-19* *** The caster can determine the locations of any ley lines up to two hexes out. If the current hex contains a ley line the caster can determine the location of the closest crosscurrent. The caster can determine the direction and distance of the targeted feature in this space.

**20-23** The caster can determine all ley lines within 3 hexes and can see all adjacent crosscurrents for any lines in the current hex. There is a chill in the air above the targeted features in this space.

**24-27** The caster can determine all ley lines within 3 hexes and can see all adjacent crosscurrents for all of those lines. Beautiful music seems to emanate from the ground in the location of all the target features within this space. 

**28-29** The caster has a complete understanding of the ley lines of this map section. The caster understands how ley lines affect spell checks and where the points of focus are located. Birds and harmless animals romp and play above the targeted mineral or geological feature.

**30-31** The caster has a complete understanding of the ley lines of this map section and the adjacent 8 map sections. The caster understands how ley lines affect spell checks and where the points of focus are located. The targeted feature or minerals are highlighted to the eyes of the caster and anyone touching the caster by pillars of blue light that shine into the heavens for 2 weeks. 

**32+** Caster knows all there is to know about ley lines and their locations worldwide. All large types of the geological feature or mineral become visible to the caster as the globe becomes transparent to the caster and anyone touching the caster. Even tiny amounts the mineral become apparent to the caster if it is close by.

# Resources
Links to external resources

- [DCC Store generator](http://juliosrpgcove.com/storegenerator/)
- Shopping items
- Monster Manuals
[Appendix M](https://appendixm.blogspot.com/)
- Town Generator
- Inns
- Mapping
- Encounter Tables

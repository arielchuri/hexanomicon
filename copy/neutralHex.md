# Neutral Hex
Choose, skip, or roll on these table. 

**Table 5-1: People and Parties**   
See DCC RPG Appendix S and T for names and titles.

1. Plague victims
2. Acolytes
3. Armed forces
4. Artisans
5. Assassin
6. Bandit
7. Berserker
8. Caravan
9. Escapees
10. Fortune Teller
11. Friar
12. Scout
13. King and entourage
14. Knight
15. Lost people
16. Magician
17. Man-at-Arms
18. Noble
19. Peasant
20. Pilgrims
21. Chain gang
22. Slavers
23. Sage
24. Traders
25. Victims
26. Witch (neutral)
27. Mysterious Traveller
28. Odd Merchant
29. Sooth Sayer
30. 0-levels looking for adventure

**Table 5-2: Settlements**   
Larger settlements will be planned sbut some smaller settlements may be come across randomly. Use a d14 if you do not want random villages or forts. 

1. Farm
2. Inn
3. Trading post
4. Mining colony
5. Outpost
6. Spider Goat Herder
7. Nomads
8. Gladiator training school
9. Dojo
10. Stronghold
11. Temple
12. Colony of friendly creatures
13. Compound 
14. Schtetl
15. Village
16. Small Fort

# Names
Create an area name based on the following rules. Optional items in ().   

#### Neutral Hex
(*Area Name*) + *Proper Name*

Examples:   
- Klazdoon
- The Sands of Dagbala
- Adaakka Desert

#### Chaotic Hex
*Special Name A* + *Area Name* + (*Proper Name*)

Examples
- The Blighted Crags
- Crooked Woods

#### Doom Hex
d100 *Special Name A* + d6 *Area Name Column* + d10-30 *Area Name* + (*Proper Name*)

Examples: *The Womb of Sadness*, *The Screaming Pits of Uruksoom*

## Name Tables

**Table 3-1: Area Names**
Choose the correct column for hex on choose *Other*.

|  d | Plains/Hills | Desert/Ice | Mountain  | Forest    | Swamp     | Other      |
|----|--------------|------------|-----------|-----------|-----------|------------|
|  1 | Plains       | Basin      | Aerie     | Cave      | Swamp     | Abyss      |
|  2 | Hills        | Desert     | Bluffs    | Cover     | Bogs      | Altar      |
|  3 | Fields       | Dunes      | Canyon    | Forest    | Pools     | Womb       |
|  4 | Grasslands   | Ocean      | Cliffs    | Jungle    | Mire      | Asylum     |
|  5 | Expanse      | Sands      | Crags     | Shadow    | Morass    | Bastion    |
|  6 | Platue       | Wastes     | Crater    | Darkness  | Moor      | Belly      |
|  7 | Belt         | Salt       | Crown     | Stand     | Fen       | Crucible   |
|  8 | Highlands    | Sea        | Dome      | Thicket   | Mud       | Dens       |
|  9 | Border       | Blight     | Eminence  | Trees     | Pits      | Dwelling   |
| 10 | Claim        | Wasteland  | Fjords    | Wildwood  | Marshland | Expanse    |
| 11 | Domain       | Emptiness  | Heights   | Wood      | Quagmire  | Eyes       |
| 12 | Zone         | Lake       | Hills     | Bramble   | Bottoms   | Void       |
| 13 | Flatland     | Expanse    | Hump      | Enclosure | Polder    | Remains    |
| 14 | Footprint    | Burning    | Mounds    | Wildness  | Quag      | Forge      |
| 15 | Lands        | Desolation | Mount     | Silvan    | Slough    | Garden     |
| 16 | Lowlands     | Glittering | Mountains | Whitewood | Swale     | Gorges     |
| 17 | The Open     | Rock       | Peaks     | Labyrinth | Swampland | Hatcheries |
| 18 | Terrain      | Wound      | Pike      | Tangle    | Holm      | End        |
| 19 | Territory    | Emptiness  | Pillars   | Boscage   | Peat Bog  | Corpse     |
| 20 | The Wild     | Anvil      | Precipice | Morass    | Marsh     | Hive       |
| 21 |              |            | Range     |           |           | Lands      |
| 22 |              |            | Ridges    |           |           | Oasis      |
| 23 |              |            | Spikes    |           |           | Pits       |
| 24 |              |            | Spires    |           |           | Plains     |
| 25 |              |            | Teeth     |           |           | Pools      |
| 26 |              |            | Towers    |           |           | Respite    |
| 27 |              |            | Vault     |           |           | Rivers     |
| 28 |              |            | Volcano   |           |           | Sanctuary  |
| 29 |              |            | Ascent    |           |           | Sea        |
| 30 |              |            | Scar      |           |           | Spikes     |

**Table 3-2: Proper Name**
Roll for prefix and suffix. Roll again for additional syllables.  

|   d | Prefix | Suffix   |
|-----|--------|----------|
|   1 | Ada    | aka      |
|   2 | Aksha  | alu      |
|   3 | Awa    | alt      |
|   4 | ba     | ax       |
|   5 | Bab    | balt     |
|   6 | Bad    | bat      |
|   7 | Bala   | char     |
|   8 | Bar    | churak   |
|   9 | Black  | dark     |
|  10 | Borah  | dook     |
|  11 | Da     | aria     |
|  12 | Dag    | bala     |
|  13 | Dil    | bane     |
|  14 | Doon   | boon     |
|  15 | Dor    | chult    |
|  16 | Dun    | dar      |
|  17 | D’     | dax      |
|  18 | Ek     | doon     |
|  19 | El     | down     |
|  20 | Erid   | dur      |
|  21 | Fal    | ech      |
|  22 | Far    | eek      |
|  23 | Fe     | eel      |
|  24 | Fell   | eno      |
|  25 | Fy     | entix    |
|  26 | Gar    | ereen    |
|  27 | Gir    | eria     |
|  28 | Glam   | fall     |
|  29 | Glom   | fell     |
|  30 | Gun    | fex      |
|  31 | Hi     | foor     |
|  32 | Hun    | gal      |
|  33 | Idle   | gar      |
|  34 | Ill    | gex      |
|  35 | Ioni   | giss     |
|  36 | Ir     | glam     |
|  37 | Isi    | glar     |
|  38 | Jai    | gloom    |
|  39 | Jod    | hex      |
|  40 | Ka     | inex     |
|  41 | Kaza   | ish      |
|  42 | Kid    | istan    |
|  43 | Klaz   | jan      |
|  44 | Kon    | jekto    |
|  45 | Kuta   | kan      |
|  46 | Kuth   | kill     |
|  47 | La     | kkam     |
|  48 | Lag    | klor     |
|  49 | Lar    | kurnix   |
|  50 | Ma     | lan      |
|  51 | Mara   | lar      |
|  52 | Mir    | lin      |
|  53 | Mont   | loloo    |
|  54 | Nadi   | mar      |
|  55 | Nag    | mare     |
|  56 | Nin    | mexy     |
|  57 | Noog   | mire     |
|  58 | Og     | nadir    |
|  59 | Oog    | neen     |
|  60 | Ool    | nipe     |
|  61 | Oor    | noop     |
|  62 | Ozy    | nun      |
|  63 | Pazur  | oop      |
|  64 | Py     | ount     |
|  65 | Quir   | ox       |
|  66 | Ra     | oz       |
|  67 | Ral    | por      |
|  68 | Rapi   | pur      |
|  68 | Ras    | queb     |
|  69 | Roon   | quell    |
|  70 | Sho    | quern    |
|  71 | Shool  | rak      |
|  72 | Shuru  | rax      |
|  73 | Sire   | reen     |
|  74 | Slo    | rez      |
|  75 | Slum   | sa       |
|  76 | Sus    | shul     |
|  77 | Tak    | sippa    |
|  78 | Tal    | slog     |
|  79 | Thule  | sloth    |
|  80 | Tuk    | sogoth   |
|  81 | Tut    | soom     |
|  82 | Tyre   | soth     |
|  83 | Ug     | sty      |
|  84 | Uli    | sylvania |
|  85 | Ur     | tear     |
|  86 | Uruk   | ton      |
|  87 | Vek    | tooth    |
|  88 | Ver    | twool    |
|  89 | Vil    | usix     |
|  90 | Vin    | vex      |
|  91 | Walden | vile     |
|  92 | Wil    | wan      |
|  93 | Xen    | weird    |
|  94 | Xi     | wood     |
|  95 | Xyl    | xene     |
|  96 | Yi     | xyl      |
|  97 | Yool   | ylon     |
|  98 | Zen    | zalu     |
|  99 | Zeph   | zoon     |
| 100 | Zir    | zzarr    |

**Table 3-3: Special Hex**

| d100 | Title       |
| ---- | ----------- |
|    1 | Abhorrent   |
|    2 | Belching    |
|    3 | Gloaming    |
|    4 | Bent        |
|    5 | Black       |
|    6 | Blasphemous |
|    7 | Blessed     |
|    8 | Blighted    |
|    9 | Blood       |
|   10 | Bogs        |
|   11 | Burn        |
|   12 | Cloud       |
|   13 | Corrupted   |
|   14 | Crooked     |
|   15 | Cruel       |
|   16 | Crystal     |
|   17 | Croaking    |
|   18 | Cursed      |
|   19 | Dark        |
|   20 | Dead        |
|   21 | Deformed    |
|   22 | Doom        |
|   23 | Dreaming    |
|   24 | Fear        |
|   25 | Fire        |
|   26 | Fiery       |
|   27 | Floating    |
|   28 | Forgetful   |
|   29 | Formless    |
|   30 | Frozen      |
|   31 | Ghost       |
|   32 | Glittering  |
|   33 | Glowing     |
|   34 | Grey        |
|   35 | Heretical   |
|   36 | Hidden      |
|   37 | Horror      |
|   38 | Ice         |
|   39 | Iron        |
|   40 | Lava        |
|   41 | Living      |
|   42 | Maw         |
|   43 | Metal       |
|   44 | Moldy       |
|   45 | Mud         |
|   46 | Obscene     |
|   47 | Pain        |
|   48 | Poison      |
|   49 | Red         |
|   50 | Rot         |
|   51 | Rotting     |
|   52 | Mourning    |
|   53 | Ruin        |
|   54 | Sadness     |
|   55 | Salt        |
|   56 | Shadowed    |
|   57 | Shifting    |
|   58 | Shroomed    |
|   59 | Demonic     |
|   60 | Shroud      |
|   61 | Smoldering  |
|   62 | Sorrow      |
|   63 | Spiked      |
|   64 | Tangled     |
|   65 | Unbidden    |
|   66 | Unborn      |
|   67 | Unholy      |
|   68 | Unseen      |
|   69 | Unspeakable |
|   70 | Wasted      |
|   71 | Weeping     |
|   72 | Whispering  |
|   73 | White       |
|   74 | Wounded     |
|   75 | Screaming   |
|   76 | Great       |
|   77 | Dire        |
|   78 | Fearsome    |
|   79 | Feeding     |
|   80 | Filthy      |
|   81 | Sunken      |
|   82 | Sordid      |
|   83 | Milky       |
|   84 | Pustulent   |
|   85 | Impurpled   |
|   86 | Corpulant   |
|   87 | Saloubriuos |
|   88 | Salivating  |
|   89 | Sinful      |
|   90 | Painted     |
|   91 | Steel       |
|   92 | Bleached    |
|   93 | Baneful     |
|   94 | Billowing   |
|   95 | Whistling   |
|   96 | Whispering  |
|   97 | Slumping    |
|   98 | Slouching   |
|   99 | Polluted    |
|  100 | Ashen       |

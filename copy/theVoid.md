# The Void
There was neither aught nor naught, nor air, nor sky beyond. What covered all? Where rested all? In watery gulf profound? Nor death was then, nor deathlessness, nor change of night and day. The One breathed calmly, self-sustained nought else beyond it lay.

Gloom, hid in gloom, existed first—one sea, eluding view. That One, a void in chaos wrapt, by inward fervour grew. Within it first arose desire, the primal germ of mind, Which nothing with existence links, as sages searching find.

The kindling ray that shot across the dark and drear abyss— Was it beneath? or high aloft? What bard can answer this? There fecundating powers were found, and mighty forces strove— A self-supporting mass beneath, and energy above.

Who knows, who ever told, from whence this vast creation rose? No gods had then been born—who then can e’er the truth disclose? Whence sprang this world, and whether framed by hand divine or no— Its lord in heaven alone can tell, if even he can show.

– The Rig Veda†

Judges of this world have little time to concern themselves with its contents. There are no great cities or wizards towers until the adventurers draw them in. The contents of hexes appear there only to serve our tale. They may be called upon as needed to provide impact to a difficult journey. They may provide choices. Do we take the long, and easy road? Or do we cut through *The Glowing bogs of Quirdoon*? This is not a setting, it is a set of tools to create a world.

Rather than a particular place that your campaign inhabits, this world is intended to provide context to and connections between the many great adventures that are available. rather than great journeys, the area immediately around the home village is considered far off land, ripe for exploration. To that end, there are no cities or kingdoms, no histories or preset locales. All that can be developed as the players progress.

These rules should be used only as needed. Rather than a fantasy world simulator, this is more of a fantasy literature simulator. If the party is making a short journey through safe and known territory, simply count off the days. On the other hand, if they are taking a shortcut through the *Fire Swamp*, then an encounter with its vile denizens is assured. If they must make a perilous journey to a far off destination, then they will face beasts, and cold, and more.

Players could start their adventure in a hex next to their village and became *Sailors on the Starless Sea*. They may proceed home from there only to find their beloved village of *Homlit* under attack by a strange hound in *Doom of the Savage Kings*. Next they journey to other nearby adventures using a hex map that is created as they travel. The hexes can be mundane or mysterious. Areas can have names and contain interesting features and creatures but only if it helps the story. The bandits may be hiding among **The Belching Crystals of Dagoor** or the party may need to take a shortcut through **The Spore Wolves of the Balax Wastes**.


## Hex Alignment
A hex may be the domain of evil and chaos. This usually reflected in the atmosphere and features of the landscape but can also be applied to the conditions, the ability to get lost, and the quantity and types of encounters. The party's actions can cause hexes to change alignment. Clearing evil from the land can make journeys safer. Dark forces could also work in the background. Causing evil to encroach ever closer. Other hexes may be the domain of goodness and law but they seem to be fewer and far between in these troubled times.

## The Process

1. The Players choose their direction of travel. Some or all of the surrounding hex contents may already be visible. Distant mountains may also be visible a few hexes off.   
2. Determine if the terrain is impassable or if there are rivers flowing between the hexes. You may also choose to have roads leading through or between hexes. 
3. Determine the alignment of new hex.
5. Determine the terrain.
6. Fill in some nearby hexes with the same terrain (all of these hexes have the same effects)
7. Name the area if needed.
8. Mark days off the calendar.
9. Determine the effects of the new hex and execute them.
  1. If there is an environmental effect, execute it. 
  2. If the hex contains beasts, determine the beasts. These beasts will always inhabit this area and attack. 
  2. If the hex is neutral, you may choose to roll for a random encounter.
